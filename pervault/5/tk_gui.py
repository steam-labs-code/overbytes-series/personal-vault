from tkinter import *
from tkinter import ttk
from vault import generate_otp, Path


# Commands ===============================
def get_otp():
    new_password = generate_otp()
    var_output.set(new_password)


# TK GUI CODE ==================================
root = Tk()
root.title('Personal Vault')

# Instantiate main windows
# root is parent of content. content is parent of main. always passing parent to child.
content_frame = ttk.Frame(root, padding=(10, 10, 10, 10))
data_frame = ttk.Frame(content_frame, borderwidth=1, relief='ridge', width=200, height=100)

# editor values stored in variables
var_output = StringVar(value='')
var_symbols = BooleanVar(value=True)
var_ascii = BooleanVar(value=True)
var_list = StringVar(value=[])

# Create objects
label_content = ttk.Label(content_frame, text='PerVault')
entry_name = ttk.Entry(content_frame)
label_output = ttk.Label(content_frame, textvariable=var_output, width=50)
button_symbols = ttk.Checkbutton(content_frame, text='Symbols', variable=var_symbols, onvalue=True)
button_ascii = ttk.Checkbutton(content_frame, text='All Ascii', variable=var_ascii, onvalue=True)
button_create = ttk.Button(content_frame, text='Generate', command=get_otp)
button_quit = ttk.Button(content_frame, text='Close', command=lambda: quit())
# TODO: switch to TreeView
listbox_files = Listbox(data_frame, listvariable=var_list, height=5)
list_scroll = ttk.Scrollbar(data_frame, orient=VERTICAL, command=listbox_files.yview)
tree = ttk.Treeview(data_frame, columns=('App Name, Username, Password'))

# Place objects on the grid
content_frame.grid(column=0, row=0, sticky=(N, S, E, W))
data_frame.grid(column=0, row=0, columnspan=3, rowspan=2, sticky=(N, S, E, W))
label_content.grid(column=3, row=0, columnspan=2, sticky=(N, W), padx=5)
label_output.grid(column=3, row=1, columnspan=2, sticky=(N, W), pady=5, padx=5)
listbox_files.grid(column=0, row=0, rowspan=3, sticky=(N, S, E, W))
list_scroll.grid(column=2, row=0, sticky=(N, S))
listbox_files['yscrollcommand'] = list_scroll.set
# entry_name.grid(column=3, row=1, columnspan=2)

button_symbols.grid(column=0, row=3)
button_ascii.grid(column=1, row=3)
button_create.grid(column=2, row=3)
button_quit.grid(column=4, row=3)

# make resizeable - positive weights required
root.columnconfigure(0, weight=1)
root.rowconfigure(0, weight=1)
content_frame.columnconfigure(0, weight=3)
content_frame.columnconfigure(1, weight=3)
content_frame.columnconfigure(2, weight=1)
content_frame.columnconfigure(3, weight=1)
data_frame.columnconfigure(0, weight=1)
data_frame.rowconfigure(0, weight=1)

if __name__ == '__main__':
    all_files = Path('database.csv')
    list_data = []
    with all_files.open() as file:
        for entry in file.readlines():
            list_data.append(entry)
    var_list.set(list_data)
    root.mainloop()
