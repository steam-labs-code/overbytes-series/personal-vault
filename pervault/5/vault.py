# Overbytes: Personal Vault
# v1.2
# https://docs.python.org/3/library/hashlib.html
import string 
import random

from pathlib import Path 


# settings
MAX_LENGTH = 32
INCLUDE_SYMBOLS = False 
SYMBOLS = '$?!<>,;'
TABLE_WIDTH = 82
COL_WIDTH = 20

database = Path('database.csv')

if not database.exists():
    database.touch()
    with database.open('w') as file:
        file.write('App Name,Username,Password\n')


# generate a otp (one time password)
def generate_otp() -> str:
    return ''.join(random.choices(string.ascii_letters + string.digits, k=MAX_LENGTH))


# generate a login
def generate_login(app_name, username: str = None,) -> str:
    """stores password"""
    password = generate_otp()

    with database.open('a') as file:
        entry = f'{app_name},{username},{password}\n'
        file.write(entry)
        return f'Saved entry.\n App: {app_name}\n Username: {username}\n Password: {password}\n' 


def display_vault():
    title = '=[ Personal Vault ]='.center(TABLE_WIDTH, '=')
    top = '\n|' + title + '|\n'
    border = '|' + '-'.center(TABLE_WIDTH, '-') + '|\n'
    bottom = '|' + '_'.center(TABLE_WIDTH, '_') + '|\n'
    table = top
    header = f'|  {"App Name".center(COL_WIDTH)} |  {"Username".center(COL_WIDTH)} | {"Password".ljust(MAX_LENGTH)} |\n'
    table += header
    table += border
    with database.open() as file:
        entries = file.readlines()[1:]
        for entry in entries:
            parts = entry.split(',')
            line = f'|{parts[0].center(COL_WIDTH)}\t| {parts[1].center(COL_WIDTH)}\t| {parts[2].strip().center(COL_WIDTH)} |\n'
            table += line
    table += bottom
    return table 


def get_entry(app_name: str) -> str:
    with database.open() as db:
        entries = db.readlines()
        for entry in entries:
            if app_name in entry:
                parts = entry.split(',')
                return f'App: {parts[0]}\nUsername: {parts[1]}\nPassword: {parts[2]}\n'
            
    return 'Not found.'


if __name__ == '__main__':
    import sys 

    options = sys.argv[1:]
    print(f'\nPersonal Vault is your offline solution for better passwords. Never store online!')
    print(f'Just run this script to generate a one-time password.\nOther options:\n\t- get\n\t- view\n\t- add\n\n')

    if len(options) >= 1:
        if options[0] == 'get':
            print(get_entry(options[1]))
        elif options[0] == 'view':
            print(display_vault())
        elif options[0] == 'add':
            print(generate_login(options[1], options[2]))
    else:
        print(f'One-time password: {generate_otp()}\n')

