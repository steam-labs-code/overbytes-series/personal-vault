import string
import random


# Get user input options
MAX_LENGTH = 32
SYMBOLS = '?!$*@=+><;:'

# Generate password
new_password = ''

# Return password
print(new_password.join(random.choices(string.ascii_letters + string.digits, k=MAX_LENGTH)))

