# Overbytes:  Personal Vault
# v1.2
import random 
import string
from pathlib import Path

database = Path('database.csv')
if not database.exists():
    database.touch()
    with database.open('w') as file:
        file.write('App Name,Username,Password\n')

# user options
MAX_LENGTH = 32
SYMBOLS = '?!$*@=+><;:'

# Utilities
def generate_password(length: int = MAX_LENGTH, symbols: bool = False):
    new_password = ''

    if length > MAX_LENGTH:
        length = MAX_LENGTH

    for i in range(length):
        if symbols:
            new_password += random.choice(string.ascii_letters + string.digits + SYMBOLS)
        else:
            new_password += random.choice(string.ascii_letters + string.digits)

    return new_password


def new_entry(entry_name: str, user_name: str = None, length: int = MAX_LENGTH, symbols: bool = False) -> str:
    new_password = ''

    if length > MAX_LENGTH:
        length = MAX_LENGTH

    for i in range(length):
        if symbols:
            new_password += random.choice(string.ascii_letters + string.digits + SYMBOLS)
        else:
            new_password += random.choice(string.ascii_letters + string.digits)

    new_entry = f'{entry_name},{user_name},{new_password}\n'
    with database.open('a') as file:
        file.write(new_entry)

    return new_password


def get_entry(entry_name) -> str:
    with database.open() as file:
        entries = file.readlines()
        for entry in entries:
            if entry_name in entry.split(','):
                return entry.split(',')

    return 'Not found.' 

