# Overbytes:  Personal Vault
# v1.2

from argparse import ArgumentParser


parser = ArgumentParser(prog='python -m vault commands options',
                        description='Personal offline password vault.')
subparsers = parser.add_subparsers(title='Vault options',
                                   help='Sub-commands', required=False)

parser.add_argument('otp',
                    action='store_true')
parser.add_argument('-l', '--length',
                    type=int,
                    default=32,
                    metavar='32',
                    help='Length of the password.')
parser.add_argument('-s', '--symbols',
                    action='store_true',
                    help='Include symbols.')
parser.add_argument('-a',
                    dest='app',
                    metavar='-a app_name',
                    help='Add a new entry to the password vault')

# Subparsers
parser_otp = subparsers.add_parser('otp', help='Generate a one-time password.')
parser_add = subparsers.add_parser('add', help='Add a new entry to the vault.')
parser_get = subparsers.add_parser('get', help='Retrieve an entry from the vault.')

parser_add.add_argument('-a',
                    dest='app',
                    metavar='-a app_name',
                    help='Add a new entry to the password vault')
parser_add.add_argument('-u',
                    dest='username',
                    metavar='username',
                    help='Username for the entry.')
parser_get.add_argument('-a',
                    dest='app',
                    metavar='app_name',
                    help='Retrieve an entry.')
