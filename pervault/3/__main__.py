from . import parser
from .vault import new_entry, get_entry, generate_password

args = parser.parse_args()
options = vars(args)

if options.get('add'):
    new_entry(args.add)
elif options.get('get'):
    print(get_entry(args.get))
elif options.get('otp'):
    print(generate_password())
