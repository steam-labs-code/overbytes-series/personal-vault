import argparse
from . import display_table, create_login, get_login, generate_password

parser = argparse.ArgumentParser()

parser.add_argument('view')
parser.add_argument('get')
parser.add_argument('-l', '--length')
options = parser.parse_args()
print(options)


if options:
    if options.view:
        print(display_table())
    elif options == 'add':
        print(create_login(options[1], options[2]))
    elif options == 'get':
        print(get_login(options[1]))
else:
    print(generate_password())