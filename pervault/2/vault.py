# Overbytes:  Personal Vault
# v1.2
import random 
import string
from pathlib import Path

# user options
MAX_LENGTH = 32
SYMBOLS = '?!$*@=+><;:'
TABLE_WIDTH = 82
COL_WIDTH = 20

database = Path('database.csv')
if not database.exists():
    database.touch()


# password generator
def generate_password(length: int = MAX_LENGTH, symbols: bool = False):
    new_password = ''

    if length > MAX_LENGTH:
        length = MAX_LENGTH

    for i in range(length):
        if symbols:
            new_password += random.choice(string.ascii_letters + string.digits + SYMBOLS)
        else:
            new_password += random.choice(string.ascii_letters + string.digits)

    return new_password


def create_login(app_name: str, username: str) -> str:
    """Add new entry to database and return it."""
    password = generate_password()
    with database.open('a') as file:
        entry = f'{app_name},{username},{password}\n'
        file.write(entry)

    return entry  


def get_login(app_name: str) -> str:
    """Return an entry from the database."""
    with database.open() as file:
        for entry in file.readlines():
            if app_name in entry:   
                return entry


def display_table() -> str:
    title = '=[ Personal Vault ]='.center(TABLE_WIDTH, '=')
    top = '\n|' + title + '|\n'
    border = '|' + '-'.center(TABLE_WIDTH, '-') + '|\n'
    bottom = '|' + '_'.center(TABLE_WIDTH, '_') + '|\n'
    table = top
    header = f'|  {"App Name".center(COL_WIDTH)} |  {"Username".center(COL_WIDTH)} | {"Password".ljust(MAX_LENGTH)} |\n'
    table += header
    table += border
    with database.open() as file:
        entries = file.readlines()[1:]
        for entry in entries:
            parts = entry.split(',')
            line = f'|{parts[0].center(COL_WIDTH)}\t| {parts[1].center(COL_WIDTH)}\t| {parts[2].strip().center(COL_WIDTH)} |\n'
            table += line
    table += bottom
    return table 


if __name__ == '__main__':
    import sys 
    import argparse
    
    print(f'\nPersonal Vault')
    options = sys.argv[1:]  # ['add', 'twitter', 'lo', '15', '-s']

    if options:
        symbols, length = options[1:]
        if options[0] == 'view':
            print(display_table())
        elif options[0] == 'add':
            print(create_login(options[1], options[2], options[3], options[4]))
        elif options[0] == 'get':
            print(get_login(options[1]))
    else:
        print(generate_password())


