# Personal Vault

Personal Vault is an application that generates and stores passwords for you. Options include:
- Generate secret
- Generate OTP
- Generate pin
- Generate passphrase
- Generate login set for a given app
- Password options: length, symbols, alpha, alphanumeric, secure
- Generate UUID

Use:
```bash

python -m vault secret
python -m vault pin
python -m vault otp
python -m vault login app_name username 20
```
