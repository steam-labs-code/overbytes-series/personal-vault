import random 
import string 
import secrets

# tool settings
MAX_LENGTH = 50
PIN_LENGTH = 8
INCLUDE_SYMBOLS = False 
SYMBOLS = '?!#@$^*><;:'


# generate otp
def generate_otp(length: int = MAX_LENGTH, has_symbols: bool = INCLUDE_SYMBOLS) -> str:
    if has_symbols:
        return ''.join(random.choices(string.ascii_letters + string.digits + SYMBOLS, k=length))
    else:
        return ''.join(random.choices(string.ascii_letters + string.digits, k=length))


# generate pin
def generate_pin(length: int = PIN_LENGTH):
    pin = ''.join(random.choices(string.digits, k=length))
    return int(pin)


# generate secure password
def generate_password():
    new_password = ''

    for i in range(MAX_LENGTH):
        new_password += secrets.choice(string.ascii_letters + string.digits)



if __name__ == '__main__':
    from sys import argv
    from collections import namedtuple

    # VaultOptions = namedtuple('VaultOptions', ['type', 'length', 'has_symbols'])
    # valid arguments: type (otp (default), pin, password), options (length, symbols)
    args = argv[1:]
    # options = VaultOptions(args)

    if len(args) > 0:
        if args[0] == 'pin':
            print(generate_pin())
        elif args[0] == 'password':
            print(generate_password())
    else:
        print(generate_otp())

